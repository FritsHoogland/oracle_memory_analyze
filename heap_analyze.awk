#!/bin/awk -f
BEGIN {
  printf "Heap dump analyzer v0.3 by Frits Hoogland\n";
  group_sum_nr=5;
  excel_mode=0;
}
{ 
  if ( $1" "$2" "$3 == "HEAP DUMP heap" ) {
    split($0,temp,"\"");
    current_heap=temp[2]; 
    heap_address[current_heap]=gensub(/\ *desc=0x([0-9a-f]*)/, "\\1", "g", temp[3]);
  }
  if ( match($1, /^parent=0x/ ) ) {
    parent_heap[current_heap]=gensub(/^parent=0x([0-9a-f]*)/, "\\1", "g", $1);
  }
  if ( $1 == "EXTENT" ) 
    if ( extent_nr_max[current_heap] == 0 || $2 > extent_nr_max[current_heap] ) extent_nr_max[current_heap] = $2;
  if ( $1 == "Chunk" ) {
    current_address=$2;
    split($0,temp,"\"");
    if ( $5 == "kghdsx" ) next;
    heap[current_address]=current_heap;
    size[current_address]=$4;
    type[current_address]=$5;
    name[current_address]=temp[2];
  }
  if ( $4 == "cpmlst" ) {
    if ( type[$1] == "perm" ) {
      split($0,temp,"\"");
      name[$1]=temp[2];
    }
  }
}
END {
  #
  # totals per heap and subheap
  #
  if ( ! excel_mode ) printf "heap and subheap size totals (all sizes in bytes)\n";
  if ( ! excel_mode ) printf "=======================================================================================================================\n";
  printf "%-16s %8s %16s %16s %16s %16s %25s\n", "heap_name", "#extents", "avg_extent_size", "#chunks", "avg_chunk_size", "sub_heap_size", "total_size";
  if ( ! excel_mode ) printf "-----------------------------------------------------------------------------------------------------------------------\n";
  for ( addr in size ) {
    total_per_heap[heap[addr]]+=size[addr];
    nrchunks[heap[addr]]+=1;
  }
  for ( heap_name in total_per_heap ) {
    if ( parent_heap[heap_name] == "" ) {
      total+=total_per_heap[heap_name]
      presentation_heap_name=heap_name;
      if ( excel_mode ) gsub(" ","_",presentation_heap_name);
      printf "%-16s %8d %16.0f %16d %16.0f %16s %25.0f\n", presentation_heap_name, extent_nr_max[heap_name]+1, total_per_heap[heap_name]/(extent_nr_max[heap_name]+1), nrchunks[heap_name], total_per_heap[heap_name]/nrchunks[heap_name], "0", total_per_heap[heap_name];
      for ( sub_heap_name in total_per_heap ) {
        if ( parent_heap[sub_heap_name] == heap_address[heap_name] ) {
          presentation_heap_name=sub_heap_name;
          if ( excel_mode ) gsub(" ","_",presentation_heap_name);
          printf " %-15s %8d %16.0f %16d %16.0f %16.0f\n", presentation_heap_name, extent_nr_max[sub_heap_name]+1, total_per_heap[sub_heap_name]/(extent_nr_max[sub_heap_name]+1), nrchunks[sub_heap_name], total_per_heap[sub_heap_name]/nrchunks[sub_heap_name], total_per_heap[sub_heap_name];
        }
      }
    }
  }
  if ( ! excel_mode ) printf "-----------------------------------------------------------------------------------------------------------------------\n";
  printf "%-16s %8s %16s %16s %16s %16s %25.0f\n\n", "total", "", "", "","", "", total;
  #
  # per heap summary of allocation reasons 
  #
  for ( addr in size ) {
    totals[heap[addr]][name[addr]]+=size[addr];
    number[heap[addr]][name[addr]]+=1;
  }
  PROCINFO["sorted_in"]="@val_num_desc";
  if ( ! excel_mode ) printf "top %d allocation by total size per alloc reason per heap\n", group_sum_nr;
  if ( ! excel_mode ) printf "==================================================================================================\n";
  for ( heap_name in totals ) {
    counter=1;
    printf "%-16s %-20s %10s %16s %3s\n", "heap", "alloc_reason", "#chunks", "total_size", "%";
    if ( ! excel_mode ) printf "--------------------------------------------------------------------------------------------------\n";
    for ( alloc_reason in totals[heap_name] ) {
      if (counter > group_sum_nr) break;
      else counter++;
      presentation_heap_name=heap_name;
      presentation_alloc_reason=alloc_reason;
      if ( excel_mode ) gsub(" ","_",presentation_heap_name);
      if ( excel_mode ) gsub(" ","_",presentation_alloc_reason);
      printf "%-16s %-20s %10d %16.0f %3d\n", presentation_heap_name, presentation_alloc_reason, number[heap_name][alloc_reason], totals[heap_name][alloc_reason], totals[heap_name][alloc_reason]/total_per_heap[heap_name]*100;
    }
  }
  printf "\n";
  #
  # per heap summary of distinct allocation reason, type and chunk size
  #
  delete totals;
  delete nrchunks;
  for ( addr in heap ) {
    totals[heap[addr]][type[addr]"!"name[addr]"!"size[addr]]+=size[addr]
    nrchunks[heap[addr]][type[addr]"!"name[addr]"!"size[addr]]+=1
  }
  if ( ! excel_mode ) printf "top %d allocations by total size per alloc reason, type, chunk size per heap\n", group_sum_nr;
  if ( ! excel_mode ) printf "==================================================================================================\n";
  for ( heap_name in totals ) {
    counter=1;
    printf "%-16s %-20s %10s %10s %16s %16s %3s\n", "heap", "alloc_reason", "#chunks", "type", "chunk_size", "total_size", "%";
    if ( ! excel_mode ) printf "--------------------------------------------------------------------------------------------------\n";
    for ( combi_row in totals[heap_name] ) {
      if (counter > group_sum_nr) break;
      else counter++;
      split(combi_row, temp, "!");
      presentation_heap_name=heap_name;
      if ( excel_mode ) gsub(" ","_",presentation_heap_name);
      if ( excel_mode ) gsub(" ","_",temp[2]);
      printf "%-16s %-20s %10d %10s %16.0f %16.0f %3d\n", presentation_heap_name, temp[2], nrchunks[heap_name][combi_row], temp[1], temp[3], totals[heap_name][combi_row], totals[heap_name][combi_row]/total_per_heap[heap_name]*100;
    }
  }
  printf "\n";    
  #
  # mimic behavior of tanel poder's heapdump analyzer
  #
  delete totals;
  delete nrchunks;
  for ( addr in heap ) {
    totals[heap[addr]"!"type[addr]"!"name[addr]"!"size[addr]]+=size[addr]
    nrchunks[heap[addr]"!"type[addr]"!"name[addr]"!"size[addr]]+=1
  }
  counter=1;
  if ( ! excel_mode ) printf "top %d allocations by total size per heap, alloc reason, type, chunk size for all heaps\n", group_sum_nr;
  if ( ! excel_mode ) printf "==================================================================================================\n";
  printf "%-16s %-20s %10s %10s %16s %16s %3s\n", "heap", "alloc_reason", "#chunks", "type", "chunk_size", "total_size", "%";
  if ( ! excel_mode ) printf "--------------------------------------------------------------------------------------------------\n";
  for ( heap_name in total_per_heap ) total_of_heaps+=total_per_heap[heap_name];
  for ( combi_row in totals ) {
    if (counter > group_sum_nr) break;
    else counter++;
    split(combi_row, temp, "!");
    if ( excel_mode ) gsub(" ","_",temp[1]);
    if ( excel_mode ) gsub(" ","_",temp[3]);
    printf "%-16s %-20s %10d %10s %16.0f %16.0f %3d\n", temp[1], temp[3], nrchunks[combi_row], temp[2], temp[4], totals[combi_row], totals[combi_row]/total_of_heaps*100;
  }
  printf "\n";
  #
  # per heap summary of chunk types
  #
  delete totals; 
  delete nrchunks;
  for ( addr in heap ) {
    totals[heap[addr]][type[addr]]+=size[addr];
    nrchunks[heap[addr]][type[addr]]+=1;
    if ( min_size[heap[addr]][type[addr]] == 0 || size[addr] < min_size[heap[addr]][type[addr]] ) min_size[heap[addr]][type[addr]]=size[addr];
    if ( size[addr] > max_size[heap[addr]][type[addr]] ) max_size[heap[addr]][type[addr]]=size[addr];
  }
  if ( ! excel_mode ) printf "sum of chunk types per heap\n";
  if ( ! excel_mode ) printf "==================================================================================================\n";
  for ( heap_name in totals ) {
    printf "%-16s %15s %10s %16s %16s %16s %3s\n", "heap", "type", "#chunks", "min_size", "max_size", "total_size", "%";
    if ( ! excel_mode ) printf "--------------------------------------------------------------------------------------------------\n";
    for ( type_name in totals[heap_name] ) {
      presentation_heap_name=heap_name;
      if ( excel_mode ) gsub(" ","_",presentation_heap_name);
      printf "%-16s %15s %10d %16.0f %16.0f %16.0f %3d\n", presentation_heap_name, type_name, nrchunks[heap_name][type_name], min_size[heap_name][type_name], max_size[heap_name][type_name], totals[heap_name][type_name], totals[heap_name][type_name]/total_per_heap[heap_name]*100;
    }
  }
  printf "\n"
  exit;
  # this prints out all the chunks
  for ( addr in heap ) 
    printf "%-16s %8d %10s %16.0f %20s\n", heap[addr], extent[addr], type[addr], size[addr], name[addr];
}
