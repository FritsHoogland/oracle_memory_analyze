#!/usr/bin/awk -f
BEGIN {
  oracle_sid = "tt";
  size = 0;
  rss = 0;
  pss = 0;
  cmd1="sudo /bin/grep -H -r -l -a -E 'ORACLE_SID=" oracle_sid ".*ORACLE_SPAWNED_PROCESS|ORACLE_SPAWNED_PROCESS.*ORACLE_SID=" oracle_sid "' /proc/*/environ | grep -v self";
  while ( ( cmd1 | getline grep_database_pid ) > 0 ) {
    #sub("^/proc/","",grep_database_pid);
    sub("/environ$","",grep_database_pid);
    cmd2="cat " grep_database_pid "/smaps";
    while ( ( cmd2 | getline cat_output ) > 0 ) {
      split( cat_output, row_contents, " ");
      if ( match( cat_output, /[0-9a-f]+-[0-9a-f]+ .... [0-9a-f]+ [0-9a-f:]+ [0-9]+\ *.*/ ) ) {
        # if size != 0, then we need to save the previous figures in the tot_... array
        if ( size != 0 ) {
          # anonymous memory by definiton doesn't display a backing file
          if ( memory_area == "" ) memory_area = "anon/bss";
          # it turns out there is anonymous memory backed by /dev/zero. this sums that with with anon/bss.
          if ( memory_area == "anon/bss" || memory_area == "/dev/zero" ) {
            tot_size["** anon/bss/zero"]+=size;
            tot_rss["** anon/bss/zero"]+=rss;
            tot_pss["** anon/bss/zero"]+=pss;
            tot_swappss["** anon/bss/zero"]+=swappss;
          }
          # save the figures for total size, rss and pss.
          tot_size[memory_area]+=size;
          tot_rss[memory_area]+=rss;
          tot_pss[memory_area]+=pss;
          tot_swappss[memory_area]+=swappss;
        }
        # at this point we declare and initalize the variables for a new section
        memory_area = row_contents[6];
        size = 0;
        rss = 0;
        pss = 0;
        swappss = 0;
      }
      # obtain figure from the appropriate row
      if ( row_contents[1] == "Size:" ) size = row_contents[2];
      if ( row_contents[1] == "Rss:" ) rss = row_contents[2];
      if ( row_contents[1] == "Pss:" ) pss = row_contents[2];
      if ( row_contents[1] == "SwapPss:" ) swappss = row_contents[2];
    }
  }
  # if size != 0, we got the figures from a memory area
  if ( size != 0 ) {
    if ( memory_area == "" ) memory_area = "anon/bss";
    if ( memory_area == "anon/bss" || memory_area == "/dev/zero" ) {
      tot_size["** anon/bss/zero"]+=size;
      tot_rss["** anon/bss/zero"]+=rss;
      tot_pss["** anon/bss/zero"]+=pss;
      tot_swappss["** anon/bss/zero"]+=swappss;
    }
    tot_size[memory_area]+=size;
    tot_rss[memory_area]+=rss;
    tot_pss[memory_area]+=pss;
    tot_swappss[memory_area]+=swappss;
  }
  # print out the figures
  printf "%12s %12s %12s %12s %-30s\n", "vsz", "rss", "pss", "swappss", "memory area";
  printf "%12s %12s %12s %12s %-30s\n", "--------", "--------", "--------", "--------","--------";
  for ( memory_area in tot_size ) {
    if ( memory_area != "** anon/bss/zero" ) {
      printf "%12.0f %12.0f %12.0f %12.0f %-30s\n", tot_size[memory_area], tot_rss[memory_area], tot_pss[memory_area], tot_swappss[memory_area], memory_area
      pss_total+=tot_pss[memory_area];
    }
  } 
  printf "%12s %12s %12s %12s %-30s\n", "--------", "--------", "--------", "--------","--------";
  printf "%12s %12s %12.0f %12s %-30s\n", "-", "-", pss_total, "-", "total";
  printf "%12s %12s %12s %12s %-30s\n", "========", "========", "========", "========", "========";
  for ( memory_area in tot_size ) {
    if ( memory_area == "** anon/bss/zero" )
      printf "%12.0f %12.0f %12.0f %12.0f %-30s\n", tot_size[memory_area], tot_rss[memory_area], tot_pss[memory_area], tot_swappss[memory_area], memory_area
  }
}

