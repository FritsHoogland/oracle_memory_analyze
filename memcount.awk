#!/bin/awk -f
BEGIN {
  size = 0;
  rss = 0;
  pss = 0;
}
{
  # start of a section
  if ( match( $0, /[0-9a-f]+-[0-9a-f]+ .... [0-9a-f]+ [0-9a-f:]+ [0-9]+\ *.*/ ) ) {
    # if size != 0, then we need to save the previous figures in the tot_... array
    if ( size != 0 ) {
      # anonymous memory by definiton doesn't display a backing file
      if ( memory_area == "" ) memory_area = "anon/bss";
      # it turns out there is anonymous memory backed by /dev/zero. this sums that with with anon/bss.
      if ( memory_area == "anon/bss" || memory_area == "/dev/zero" ) {
        tot_size["** anon/bss/zero"]+=size;
        tot_rss["** anon/bss/zero"]+=rss;
        tot_pss["** anon/bss/zero"]+=pss;
      }
      # save the figures for total size, rss and pss.
      tot_size[memory_area]+=size;
      tot_rss[memory_area]+=rss;
      tot_pss[memory_area]+=pss;
    }
    # at this point we declare and initalize the variables for a new section
    memory_area = $6
    size = 0;
    rss = 0;
    pss = 0;
  }
  # obtain figure from the appropriate row
  if ( $1 == "Size:" ) size = $2;
  if ( $1 == "Rss:" ) rss = $2;
  if ( $1 == "Pss:" ) pss = $2;
} 
END {
  # if size != 0, we got the figures from a memory area
  if ( size != 0 ) {
    if ( memory_area == "" ) memory_area = "anon/bss";
    if ( memory_area == "anon/bss" || memory_area == "/dev/zero" ) {
      tot_size["** anon/bss/zero"]+=size;
      tot_rss["** anon/bss/zero"]+=rss;
      tot_pss["** anon/bss/zero"]+=pss;
    }
    tot_size[memory_area]+=size;
    tot_rss[memory_area]+=rss;
    tot_pss[memory_area]+=pss;
  }
  # print out the figures
  print "tot", "\t", "rss", "\t", "pss", "\t", "memory area";
  for ( memory_area in tot_size ) 
    print tot_size[memory_area], "\t", tot_rss[memory_area], "\t", tot_pss[memory_area], "\t", memory_area
}
