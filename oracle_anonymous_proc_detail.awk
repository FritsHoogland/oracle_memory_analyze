#!/usr/bin/awk -f
BEGIN {
  PROCINFO["sorted_in"] = "@ind_num_asc";
  pid="3441"
  cmd="cat /proc/" pid "/smaps";
  while ( ( cmd | getline cat_output ) > 0 ) {
    split( cat_output, row_contents, " ");
    if ( match( cat_output, /[0-9a-f]+-[0-9a-f]+ .... [0-9a-f]+ [0-9a-f:]+ [0-9]+\ *.*/ ) ) {
      if ( size != 0 ) {
        memory_area[start_addr]=display_name;
        start_address[start_addr]=start_addr;
        end_address[start_addr]=end_addr;
        array_size[start_addr]=size;
        array_rss[start_addr]=rss;
        array_pss[start_addr]=pss;
      }
      display_name=row_contents[6];
      if ( display_name == "" ) display_name = "bss/anon";
      start_addr=row_contents[1];
      sub("-[0-9a-f]+$","",start_addr);
      end_addr=row_contents[1];
      sub("^[0-9a-f]+-","",end_addr);
      size=0;
      rss=0;
      pss=0;
    }
    if ( display_name == "bss/anon" || display_name == "/dev/zero" ) {
      if ( row_contents[1] == "Size:" ) size=row_contents[2]; 
      if ( row_contents[1] == "Rss:" ) rss=row_contents[2];
      if ( row_contents[1] == "Pss:" ) pss=row_contents[2];
    }
  }
  # print out the figures
  printf "%16s %16s %12s %12s %12s %12s\n", "start addr", "end addr", "type", "vsz", "rss", "pss";
  printf "%16s %16s %12s %12s %12s %12s\n", "-----", "-----", "-----", "-----", "-----", "-----";
  for ( start_addr in start_address ) {
    printf "%16s %16s %12s %12.0f %12.0f %12.0f\n", start_address[start_addr], end_address[start_addr], memory_area[start_addr], array_size[start_addr], array_rss[start_addr], array_pss[start_addr];
  }
}
