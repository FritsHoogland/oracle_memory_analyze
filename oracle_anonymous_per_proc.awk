#!/usr/bin/awk -f
BEGIN {
  PROCINFO["sorted_in"] = "@ind_str_asc";
  oracle_sid = "tt";
  oracle_home = "/u01/app/oracle/product/19.4.0.0/dbhome_1";
  process_name = "";
  process_pid = "";
  memory_area = "**begin";
  cmd1="sudo /bin/grep -H -r -l -a -E 'ORACLE_SID=" oracle_sid "' /proc/*/environ | grep -v self";
  while ( ( cmd1 | getline grep_database_pid ) > 0 ) {
    sub("/environ$","",grep_database_pid);
    cmd2="readlink " grep_database_pid "/exe";
    cmd2 | getline executable;
    if ( executable == oracle_home "/bin/oracle" ) {
      process_pid=grep_database_pid;
      sub("^/proc/","",process_pid);
      cmd2="cat " grep_database_pid "/cmdline | strings";
      cmd2 | getline proc_name;
      display_name = proc_name " " process_pid;
      #display_name = process_name "%" process_pid;
      cmd3="cat " grep_database_pid "/smaps";
      while ( ( cmd3 | getline cat_output ) > 0 ) {
        split( cat_output, row_contents, " ");
        if ( match( cat_output, /[0-9a-f]+-[0-9a-f]+ .... [0-9a-f]+ [0-9a-f:]+ [0-9]+\ *.*/ ) ) {
          if ( size != 0 ) {
            if ( memory_area == "" ) tot_anon_bss[display_name]+=pss;
            if ( memory_area == "/dev/zero" ) tot_dev_zero[display_name]+=pss;
          }
          memory_area = row_contents[6];
          pss = 0;
          size = 0;
        }
        if ( row_contents[1] == "Size:" ) size = row_contents[2];
        if ( row_contents[1] == "Pss:" ) pss = row_contents[2];
      }
    }
  }
  # if size != 0, we got the figures from a memory area, which could be anonymous or /dev/zero
  if ( size != 0 ) {
    if ( memory_area == "" ) tot_anon_bss[display_name]+=pss;
    if ( memory_area == "/dev/zero" ) tot_dev_zero[display_name]+=pss;
  }
  # print out the figures
  printf "%40s %12s %12s %12s\n", "process", "anon", "/dev/zero", "tot"
  printf "%40s %12s %12s %12s\n", "-------", "-------", "-------", "-------";
  for ( ind in tot_anon_bss ) {
    printf "%40s %12.0f %12.0f %12.0f\n", ind, tot_anon_bss[ind], tot_dev_zero[ind], tot_anon_bss[ind]+tot_dev_zero[ind];
  }
}

